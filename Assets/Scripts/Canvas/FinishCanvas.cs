using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinishCanvas : MonoBehaviour
{
    public GameObject FinishScreen;
    public Text FinishText;

    public void ShowFinishScreen(string winner, int shoots)
    {
        FinishScreen.SetActive(true);
        FinishText.text = $"Vencedor: {winner}.\n Numero de disparos: {shoots}.";
    }

    public void HideFinishScreen()
    {
        FinishScreen.SetActive(false);
    }

    public void Reload()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
