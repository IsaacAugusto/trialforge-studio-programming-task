using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;

namespace GameEvents
{
    [CreateAssetMenu]
    public class GameEvent : ScriptableObject
    {
        public List<GameEventListeners> Listeners = new List<GameEventListeners>();

        public void Raise(Object context = null)
        {
            for (int i = Listeners.Count-1; i >= 0; i--)
            {
                Listeners[i].OnEventRaised(context);
            }
        }

        public void RegisterListener(GameEventListeners listener)
        {
            Listeners.Add(listener);
        }

        public void RemoveListerner(GameEventListeners listener)
        {
            Listeners.Remove(listener);
        }
    }
}
