using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Object = System.Object;

namespace GameEvents
{
    public class GameEventListeners : MonoBehaviour
    {
        public GameEvent Event;
        public UnityEvent<Object> Response;

        private void OnEnable()
        {
            Event.RegisterListener(this);
        }

        private void OnDisable()
        {
            Event.RemoveListerner(this);
        }

        public void OnEventRaised(Object context)
        {
            Response?.Invoke(context);
        }
    }
}
