using UnityEngine;

[System.Serializable]
public class RangedFloat
{
    public float MaxValue = 1;
    public float MinValue = 1;

    private float _savedRangom = 1;

    public RangedFloat()
    {

    }

    public RangedFloat(RangedFloat rangedFloat)
    {
        MaxValue = rangedFloat.MaxValue;
        MinValue = rangedFloat.MinValue;

        _savedRangom = rangedFloat._savedRangom;
    }

    public float GetRandom()
    {
        return Random.Range(MinValue, MaxValue);
    }

    public void TrowSavedRandom()
    {
        _savedRangom = Random.Range(MinValue, MaxValue);
    }

    public float GetSavedRandom()
    {
        return _savedRangom;
    }
}
