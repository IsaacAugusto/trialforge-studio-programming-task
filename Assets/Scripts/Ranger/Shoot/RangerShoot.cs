using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvents;
using GenericObjectPool;

public class RangerShoot : MonoBehaviour
{
    public GameEvent OnShootEvent;
    public GameEvent OnAimEvent;


    [SerializeField] private Transform _projectileExitPoint;

    private GenericPool _projectilePool;
    private RangerEntity _entity;

    private bool _chargingProjectile;
    private float _chargeForce;
    private Vector2 _launchDirection;
    private bool _aiming = false;
    private BowAimController _bowAim;

    private float _chargeSpeed = 10;
    private float _maxForce = 30;
    private int _numberOfShoots = 0;

    void Awake()
    {
        _entity = GetComponentInChildren<RangerEntity>();
        _bowAim = GetComponentInChildren<BowAimController>();
        _projectilePool = GetComponentInChildren<GenericPool>();
    }

    void Update()
    {
        DetectShootInput();
    }

    public void StartTurn()
    {
        _projectilePool.PoolSO.InitSpawnCount();
        _aiming = true;
        _chargeForce = 0;
        _bowAim.StartAiming();
        OnAimEvent?.Raise();
    }

    public int GetShootsNumber()
    {
        return _numberOfShoots;
    }

    public bool GetAiming()
    {
        return _aiming;
    }

    public float GetMaxForce()
    {
        return _maxForce;
    }

    public float GetChargeForce()
    {
        return _chargeForce;
    }

    public bool GetCharging()
    {
        return _chargingProjectile;
    }

    private void DetectShootInput()
    {
        if (_aiming)
        {
            if (_entity.Input().ShootPressed)
            {
                _chargingProjectile = true;
                _chargeForce = 0;
                _launchDirection = _entity.Input().AimDirection.normalized;
                _bowAim.LockAiming();
                SoundManager.Instance?.PlaySound(SoundManager.Instance.Sounds.BowChargeSound);
            }

            if (_entity.Input().ShootHolded && _chargingProjectile)
            {
                if (_chargeForce < _maxForce)
                {
                    _chargeForce += Time.deltaTime * _chargeSpeed;
                }
                else
                {
                    _chargeForce = _maxForce;
                }
            }

            ShootInfoCanvasManager.Instance?.SetAngleAndForce(Vector2.SignedAngle(transform.right, _chargingProjectile? _launchDirection : _entity.Input().AimDirection.normalized), _chargeForce);

            if (_entity.Input().ShootReleased && _chargingProjectile)
            {
                //shoot
                SoundManager.Instance?.StopSounds();
                SoundManager.Instance?.PlaySound(SoundManager.Instance.Sounds.ArrowShootSound);
                _numberOfShoots++;
                _chargingProjectile = false;
                _aiming = false;
                Projectile proj = _projectilePool.PoolSO.Get().GetComponent<Projectile>();
                proj.transform.position = _projectileExitPoint.position;
                proj.SetProjectileData(_launchDirection, _chargeForce);
                FindObjectOfType<CameraManager>()?.SetVelocity(0f);
                FindObjectOfType<CameraManager>()?.SetOffset(Vector2.zero);
                FindObjectOfType<CameraManager>()?.SetTarget(proj.transform);
                OnShootEvent?.Raise();
            }

        }
    }
}
