using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitArea : MonoBehaviour, IDamageble
{
    public float DamageMultiplier = 1;
    private RangerEntity _entity;

    private void Awake()
    {
        _entity = GetComponentInParent<RangerEntity>();
    }

    public void GetDamage(float amount)
    {
        _entity.GetDamage(amount * DamageMultiplier);
    }
}
