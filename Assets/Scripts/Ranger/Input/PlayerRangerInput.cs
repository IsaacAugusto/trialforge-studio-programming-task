using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRangerInput : RangerEntityInput
{
    void Start()
    {
        
    }

    public override void GetAimDirectionInput()
    {
        if (Input.GetJoystickNames().Length > 0 && !Input.GetJoystickNames()[0].Equals(string.Empty))
        {
            AimDirection = new Vector2(Input.GetAxis("HorizontalAim"), Input.GetAxis("VerticalAim"));
        }
        else
        {
            AimDirection = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position).normalized;
        }
    }

    public override void GetShootInput()
    {
        ShootPressed = Input.GetButtonDown("Fire1");
        ShootHolded = Input.GetButton("Fire1");
        ShootReleased = Input.GetButtonUp("Fire1");
    }
}
