using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RangerEntityInput : MonoBehaviour
{
    [HideInInspector]
    public Vector2 AimDirection;
    [HideInInspector]
    public bool ShootPressed;
    [HideInInspector]
    public bool ShootHolded;
    [HideInInspector]
    public bool ShootReleased;

    public abstract void GetAimDirectionInput();
    public abstract void GetShootInput();

    protected virtual void Update()
    {
        GetAimDirectionInput();
        GetShootInput();
    }
}
