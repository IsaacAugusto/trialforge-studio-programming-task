using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IARangerInput : RangerEntityInput
{
    public IABrainSO Brain;
    
    private Transform _enemy;

    private float _shootForce = 30;

    private void Start()
    {
        var rangers = FindObjectsOfType<RangerEntityInput>();
        foreach (var ranger in rangers)
        {
            if (ranger.transform != transform)
            {
                _enemy = ranger.transform;
            }
        }
        Brain.ResetVariations();
    }

    public override void GetAimDirectionInput()
    {
        AimDirection = Brain.GetAim(transform.position, _enemy.position, _shootForce);
    }

    public void SetForceAndVariations(float value)
    {
        Brain.TrowVariations();
        _shootForce = value;
    }

    public override void GetShootInput()
    {
        
    }
}
