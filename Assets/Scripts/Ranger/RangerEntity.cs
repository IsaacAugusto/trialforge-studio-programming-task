using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvents;
using EventsArg;

public class RangerEntity : MonoBehaviour
{
    public int PlayerID;
    public string Name = "Arqueiro";
    public float MaxHealth = 100;
    public float Health { get => _healt; }

    public GameEvent OnDamage;
    public GameEvent OnDefeated;


    private float _healt;
    private RangerShoot _rangerShoot;
    private RangerEntityInput _entityInput;


    private void Awake()
    {
        _rangerShoot = GetComponentInChildren<RangerShoot>();
        _entityInput = GetComponentInChildren<RangerEntityInput>();
    }

    public RangerEntityInput Input()
    {
        return _entityInput;
    }

    private void Start()
    {
        _healt = MaxHealth;
    }

    public void StartTurn()
    {
        _rangerShoot.StartTurn();
    }

    public int GetShootsNumber()
    {
        return _rangerShoot.GetShootsNumber();
    }

    public void GetDamage(float amount)
    {
        _healt -= amount;
        OnDamage.Raise(new DamageData(amount, this));
        if (_healt <= 0)
            GetDefeated();
    }

    public void GetDefeated()
    {
        OnDefeated.Raise(new EndGameData(this));
    }
}
