using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GenericObjectPool{

    public class GenericPool : MonoBehaviour
    {
        public GenericObjectPoolSO PoolSO;

        private void OnDestroy()
        {
            PoolSO.ResetQueue();
        }
    }

}
