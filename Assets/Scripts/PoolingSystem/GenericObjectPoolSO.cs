using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GenericObjectPool{

    [CreateAssetMenu(menuName = "ObjectPoolScriptables/Generic", fileName = "GenericPool")]
    public class GenericObjectPoolSO : ScriptableObject
    {
        public GameObject ObjectToPooled;

        [Range(0, 100)]
        public int SpawnLimit;

        private int _spawnCount = 0;

        protected Queue<GameObject> TQueue = new Queue<GameObject>();

        public void InitSpawnCount(){
            _spawnCount = 0;
        }

        public virtual GameObject Get(Transform parentTransform = null){
            if (TQueue.Count > 0){
                return TQueue.Dequeue();
            }else if (SpawnLimit == 0){
                Add(1, parentTransform);
                return TQueue.Dequeue();
            }else if (_spawnCount < SpawnLimit){
                Add(1, parentTransform);
                return TQueue.Dequeue();
            }else{
                return null;
            }
        }

        protected virtual void Add(int quant, Transform parentTransform){
            for (int i = 0; i < quant; i++){
                GameObject objectPooled = parentTransform == null? Instantiate(ObjectToPooled) : Instantiate(ObjectToPooled, parentTransform);
                _spawnCount++;
                if (objectPooled.GetComponent<PooledObject>() == null){
                    objectPooled.AddComponent<PooledObject>().Pool = this;
                }
                TQueue.Enqueue(objectPooled);
            }
        }

        public virtual void GetPooled(GameObject go){
            TQueue.Enqueue(go);
        }

        public void ResetQueue()
        {
            TQueue = new Queue<GameObject>();
        }
    }

}
