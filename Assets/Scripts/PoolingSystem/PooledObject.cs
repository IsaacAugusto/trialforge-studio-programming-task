using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GenericObjectPool{

    public class PooledObject : MonoBehaviour
    {
        public GenericObjectPoolSO Pool;

        void OnDisable(){
            GetQueued();
        }

        public void GetQueued(){
            Pool.GetPooled(gameObject);
        }
    }

}
