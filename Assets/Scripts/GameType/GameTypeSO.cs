using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameTypeEnum {JxJ, JxC, CxC}

[CreateAssetMenu]
public class GameTypeSO : ScriptableObject
{
    public GameTypeEnum GameType;
    public GameObject Player1GO;
    public GameObject Player2GO;

    public void CreateGame(List<RangerEntity> playersList)
    {
        var player1 = Instantiate(Player1GO, Vector2.left * Random.Range(15, 25), Quaternion.identity).GetComponent<RangerEntity>();
        var player2 = Instantiate(Player2GO, Vector2.right * Random.Range(15, 25), Quaternion.Euler(0, 180, 0)).GetComponent<RangerEntity>();

        player1.PlayerID = 0;
        player2.PlayerID = 1;

        playersList[0] = player1;
        playersList[1] = player2;
    }
}
