using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SelectedGameType
{
    public static GameTypeSO SelectedType;

    public static void SetSelectedType(GameTypeSO type)
    {
        SelectedType = type;
    }

    public static GameTypeSO GetSelectedType()
    {
        return SelectedType;
    }
}
