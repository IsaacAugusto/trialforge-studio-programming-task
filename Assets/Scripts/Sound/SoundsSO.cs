using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Sounds")]
public class SoundsSO : ScriptableObject
{
    public AudioClip ClickSound;
    public AudioClip BowChargeSound;
    public AudioClip ArrowShootSound;
    public AudioClip ArrowHitSound;

}
