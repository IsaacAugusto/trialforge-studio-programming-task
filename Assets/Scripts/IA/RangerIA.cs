using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangerIA : MonoBehaviour
{
    public RangerEntityInput MyInput;
    public RangerShoot Shoot;

    private float desigredCharge = 30;

    void Update()
    {
        if (Shoot.GetCharging())
        {
            MyInput.ShootPressed = false;
            if (Shoot.GetChargeForce() < desigredCharge || Shoot.GetChargeForce() >= Shoot.GetMaxForce())
            {
                return;
            }
            else
            {
                Release();
            }
        }
    }

    private void LateUpdate()
    {

    }

    public void SetRandomCharge()
    {
        desigredCharge = Random.Range(20, 30);
        (MyInput as IARangerInput).SetForceAndVariations(desigredCharge);
    }

    public void StartCharging()
    {
        if (Shoot.GetAiming())
        {
            Invoke("Charge", 3f);
        }
    }

    private void ReleaseDelayed()
    {
        MyInput.ShootReleased = false;
        MyInput.ShootHolded = false;
        MyInput.ShootPressed = false;
    }

    private void Charge()
    {
        MyInput.ShootPressed = true;
        MyInput.ShootHolded = true;
    }

    private void Release()
    {
        MyInput.ShootReleased = true;
        MyInput.ShootHolded = false;
        MyInput.ShootPressed = false;
        Invoke("ReleaseDelayed", .5f);
    }

}
