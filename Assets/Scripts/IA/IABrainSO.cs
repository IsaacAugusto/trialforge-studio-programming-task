using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class IABrainSO : ScriptableObject
{
    public RangedFloat ForceVariation;
    public RangedFloat AngleVariarion;

    [Header("Velocidade de melhora da IA")]
    public float IncrementalStep = .05f;

    private RangedFloat forceV;
    private RangedFloat angleV;

    public void ResetVariations()
    {
        forceV = new RangedFloat(ForceVariation);
        angleV = new RangedFloat(AngleVariarion);
    }

    private void IncrementIA()
    {
        if (forceV.MaxValue > 1)
        {
            forceV.MaxValue -= .05f;
            if (forceV.MaxValue < 1)
            {
                forceV.MaxValue = 1;
            }
        }

        if (forceV.MinValue < 1)
        {
            forceV.MinValue += .05f;
            if (forceV.MinValue > 1)
            {
                forceV.MinValue = 1;
            }
        }

        if (angleV.MaxValue > 1)
        {
            angleV.MaxValue -= .05f;
            if (angleV.MaxValue < 1)
            {
                angleV.MaxValue = 1;
            }
        }

        if (angleV.MinValue < 1)
        {
            angleV.MinValue += .05f;
            if (angleV.MinValue > 1)
            {
                angleV.MinValue = 1;
            }
        }
    }

    public void TrowVariations()
    {

        IncrementIA();
        forceV.TrowSavedRandom();
        angleV.TrowSavedRandom();
    }

    public Vector2 GetAim(Vector2 enemyPos, Vector2 entityPos, float force)
    {
        Vector2 result = (entityPos - enemyPos).normalized;

        float distance = Vector2.Distance(entityPos, enemyPos);
        float speed = force == 0 ? 30 : force * forceV.GetSavedRandom();

        float gravity = Mathf.Abs(Physics2D.gravity.magnitude);
        float angle = 0.5f * (Mathf.Asin((gravity * distance) / (speed * speed)));

        float degAngle = (angle * Mathf.Rad2Deg) * angleV.GetSavedRandom();

        if (enemyPos.x > entityPos.x)
            degAngle = -degAngle;

        if (float.IsNaN(degAngle) || Mathf.Abs(degAngle) > 80)
        {
            if (enemyPos.x > entityPos.x)
                degAngle = -45;
            else
                degAngle = 45;
        }

        result = Quaternion.Euler(0, 0, degAngle) * result;

        return result;
    }
}
