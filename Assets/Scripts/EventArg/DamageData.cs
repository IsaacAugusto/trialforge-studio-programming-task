using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EventsArg
{
    public class DamageData
    {
        public float DamageAmount;
        public RangerEntity PlayerDamaged;

        public DamageData(float amount, RangerEntity entity)
        {
            DamageAmount = amount;
            PlayerDamaged = entity;
        }
    }
}