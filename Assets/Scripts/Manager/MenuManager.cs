using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [HideInInspector]
    public GameTypeSO GameType;

    public GameObject RangerJogador;
    [HideInInspector]
    public GameObject RangerIA;

    public void SelectGameType(GameTypeSO type)
    {
        GameType = type;
    }

    public void SelectDificulty(GameObject EnemyIA)
    {
        RangerIA = EnemyIA;
        CreateGame();
    }

    public void QuitApp()
    {
        Application.Quit();
    }

    public void PlayClickSound()
    {
        SoundManager.Instance?.PlaySound(SoundManager.Instance.Sounds.ClickSound);
    }

    public void CreateGame()
    {
        Debug.Log(GameType.GameType);
        switch (GameType.GameType)
        {
            case GameTypeEnum.JxJ:
                GameType.Player1GO = RangerJogador;
                GameType.Player2GO = RangerJogador;
                break;
            case GameTypeEnum.JxC:
                GameType.Player1GO = RangerJogador;
                GameType.Player2GO = RangerIA;
                break;
            case GameTypeEnum.CxC:
                GameType.Player1GO = RangerIA;
                GameType.Player2GO = RangerIA;
                break;
            default:
                GameType.Player1GO = RangerJogador;
                GameType.Player2GO = RangerJogador;
                break;
        }

        SelectedGameType.SelectedType = GameType;
        SceneManager.LoadScene("GameScene");
    }
}
