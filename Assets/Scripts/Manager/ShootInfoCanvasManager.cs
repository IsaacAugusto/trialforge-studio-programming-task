using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShootInfoCanvasManager : MonoBehaviour
{
    public static ShootInfoCanvasManager Instance;

    [SerializeField] private GameObject _shootInfoCanvas;
    [SerializeField] private Text _angle;
    [SerializeField] private Text _force;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }else if (Instance != this)
        {
            Destroy(this);
        }
    }

    public void ShowInfo()
    {
        _shootInfoCanvas.SetActive(true);
    }

    public void HideInfo()
    {
        _shootInfoCanvas.SetActive(false);
    }

    public void SetAngleAndForce(float angle, float force)
    {
        _angle.text = angle.ToString("F2") + "�";
        _force.text = force.ToString("F1");
    }
}
