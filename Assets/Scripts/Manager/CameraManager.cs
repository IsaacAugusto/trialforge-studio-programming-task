using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvents;

public class CameraManager : MonoBehaviour
{
    private Transform _target;
    private bool _isChasingTarget;
    private Vector2 _velRef;
    private float _vel;
    private Vector2 _offSet;

    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (_isChasingTarget && _target != null)
        {
            FollowTarget();
        }
    }

    public void SetVelocity(float vel)
    {
        _vel = vel;
    }

    public void SetOffset(Vector2 offset)
    {
        _offSet = offset;
    }

    public void SetTarget(Transform target)
    {
        _target = target;
    }

    public void StartFollowingTarget()
    {
        _isChasingTarget = true;
    }

    private void FollowTarget()
    {
        transform.position = Vector2.SmoothDamp(transform.position, (Vector2)_target.position + _offSet, ref _velRef, _vel);
    }
}
