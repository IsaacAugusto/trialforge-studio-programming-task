using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using GameEvents;
using Object = System.Object;

public class GameManager : MonoBehaviour
{
    public bool GameIsRunning;
    public List<RangerEntity> Players = new List<RangerEntity>(2);
    public GameEvent OnStartTurn;

    [SerializeField] private FinishCanvas _finishCanvas;
    private CameraManager _camera;
    private RangerEntity _currentTurnPlayer;

    public GameTypeSO teste;

    private void Awake()
    {
        _camera = FindObjectOfType<CameraManager>();
        StartGame(SelectedGameType.GetSelectedType());
    }

    void Start()
    {
        _currentTurnPlayer = Players[1];
        ChangeTurn();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame(GameTypeSO gameType)
    {
        gameType.CreateGame(Players);
        GameIsRunning = true;
    }

    public void EndGame(Object _endGameData)
    {
        GameIsRunning = false;

        RangerEntity looser = ((EndGameData)_endGameData).Looser;
        RangerEntity winner = looser.PlayerID == 0? Players[1] : Players[0];
        StartCoroutine(ShowFinishCanvas(winner));
    }

    private IEnumerator ShowFinishCanvas(RangerEntity winner)
    {
        yield return new WaitForSeconds(1.5f);
        _finishCanvas.ShowFinishScreen(winner.Name, winner.GetShootsNumber());
    }

    public void ChangeTurn()
    {
        if (!GameIsRunning) { return; }

        _currentTurnPlayer = _currentTurnPlayer.PlayerID == 0 ? Players[1] : Players[0];
        _camera.SetTarget(_currentTurnPlayer.transform);
        _camera.SetOffset(Vector2.up * 3);
        _camera.SetVelocity(1f);
        _camera.StartFollowingTarget();
        Invoke("StartPlayerTurn", 3f);
    }

    private void StartPlayerTurn()
    {
        OnStartTurn.Raise();
        _currentTurnPlayer.StartTurn();
    }
}
