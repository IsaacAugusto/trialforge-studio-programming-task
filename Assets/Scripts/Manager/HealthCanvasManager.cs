using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EventsArg;
using Object = System.Object;

public class HealthCanvasManager : MonoBehaviour
{
    [SerializeField] private Image[] _playersHealthBar = new Image[2];

    private void SetHealtBarFill(float amount, int playerID)
    {
        _playersHealthBar[playerID].fillAmount = amount;
    }

    public void PlayerGetDamage(Object data)
    {
        DamageData damageData = (DamageData)data;
        SetHealtBarFill(damageData.PlayerDamaged.Health / damageData.PlayerDamaged.MaxHealth, damageData.PlayerDamaged.PlayerID);
    }
}
