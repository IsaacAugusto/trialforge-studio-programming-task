using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowAimController : MonoBehaviour
{
    private bool _aiming = false;
    private RangerEntityInput _input;
    private SpriteRenderer _renderer;
    [SerializeField] private Sprite _noArrowSprite;
    [SerializeField] private Sprite _withArrowSprite;

    void Awake()
    {
        _input = GetComponentInParent<RangerEntityInput>();
        _renderer = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_aiming)
        {
            transform.right = _input.AimDirection;
        }
    }

    public void LockAiming()
    {
        _aiming = false;
    }

    public void StartAiming()
    {
        ReloadArrowSprite();
        _aiming = true;
    }

    public void EndAiming()
    {
        ShootSpriteChange();
        _aiming = false;
    }


    public void ReloadArrowSprite()
    {
        _renderer.sprite = _withArrowSprite;
    }

    public void ShootSpriteChange()
    {
        _renderer.sprite = _noArrowSprite;
    }
}
