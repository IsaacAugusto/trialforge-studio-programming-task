using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvents;
using GenericObjectPool;

public class Projectile : MonoBehaviour
{
    public GameEvent OnEndHit;
    public float DamagePoints;
    private Rigidbody2D _rb;
    private bool _onAir;
    private RaycastHit2D _hit;
    private float _rayDuration;
    private PooledObject _pooled;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _rayDuration = GetComponent<Collider2D>().bounds.extents.x;
        _pooled = GetComponent<PooledObject>();
    }

    private void Update()
    {
        if (_onAir)
        {
            transform.right = _rb.velocity.normalized;
            CastHit();
        }
    }

    public void SetProjectileData(Vector2 launchDir, float launchForce)
    {
        ResetProjectile();
        _rb.AddForce(launchDir * launchForce, ForceMode2D.Impulse);
    }

    private void ResetProjectile()
    {
        gameObject.SetActive(true);
        _onAir = true;
        _rb.constraints = RigidbodyConstraints2D.None;
    }

    private void CastHit()
    {
        _hit = Physics2D.Raycast(transform.position, transform.right, _rayDuration * 1.1f);
        if (_hit)
        {
            if (_hit.transform.tag.Equals("Hittable"))
            {
                _onAir = false;
                _rb.velocity = Vector2.zero;
                _rb.constraints = RigidbodyConstraints2D.FreezeAll;
                if (_hit.transform.GetComponent<IDamageble>() != null)
                {
                    _hit.transform.GetComponent<IDamageble>().GetDamage(DamagePoints);
                }
                Invoke("ReturnToPool", 15);
                SoundManager.Instance?.PlaySound(SoundManager.Instance.Sounds.ArrowHitSound);
                OnEndHit?.Raise();
            }
        }
    }

    private void ReturnToPool()
    {
        gameObject.SetActive(false);
    }
}
